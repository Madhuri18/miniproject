package com.agrotrading;

import org.springframework.boot.SpringApplication;

import org.springframework.boot.autoconfigure.SpringBootApplication;




@SpringBootApplication

public class AgroTradingSystemApplication {

	public static void main(String[] args) {
		SpringApplication.run(AgroTradingSystemApplication.class, args);
	}

}
